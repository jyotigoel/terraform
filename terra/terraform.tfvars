vpc_id = "11.0.0.0/16"
vpc_tags = {
  Name    = "Nginx-vpc",
  Owner   = "Jyoti Goel",
  purpose = "os server"
}
public_subnet_cidr = "11.0.192.0/19"
public_subnet_tags = {
  Name    = "os-subnet",
  Owner   = "Jyoti Goel",
  purpose = "os manager"
}
private_subnet1_cidr = "11.0.160.0/19"
private_subnet1_tags = {
  Name    = "data-node1",
  Owner   = "Jyoti Goel",
  purpose = "os data node"
}
private_subnet2_cidr = "11.0.128.0/19"
private_subnet2_tags = {
  Name    = "data-node2",
  Owner   = "Jyoti Goel",
  purpose = "os data node"
}
pvt_sg_tags = {
  Name    = "OS-engine-private-sg"
  Owner   = "Jyoti Goel"
  purpose = "OS-Engine"
}
pb_sg_tags = {
  Name    = "OS-engine-public-sg"
  Owner   = "Jyoti Goel"
  purpose = "OS-Engine"
}
public_subnet_zone   = "us-east-1a"
private_subnet1_zone = "us-east-1b"
private_subnet2_zone = "us-east-1c"
ami= "ami-00d06b16309e6e220"
manager_tags = {
    Name    = "OS-Manager",
    Owner   = "Jyoti Goel",
    purpose = "os-manager"
  }
node1_tags =  {   
    Name    = "OS-Data-Node1",
    Owner   = "Jyoti Goel",
    purpose = "os-data-node"
}
node2_tags =  {   
    Name    = "OS-Data-Node2",
    Owner   = "Jyoti Goel",
    purpose = "os-data-node"
}
access_key = "OpenSearch"
key_path = "/home/jyoti/Downloads/Key-pair.pem"
