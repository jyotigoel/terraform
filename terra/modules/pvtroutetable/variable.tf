variable "private_routeTable_tags" {
  type        = map(string)
  description = "tags for Nginx pvt_rtTable vpc"
  default = {
    Name    = "os-private-routeTB",
    Owner   = "Rishabh Arora",
    purpose = "os data node"

  }
}
variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Nginx vpc"
}
variable "igw_id" {
  default     = ""
  type        = string
}
