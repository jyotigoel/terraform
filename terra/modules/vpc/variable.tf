variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Nginx vpc"
}
variable "vpc_tags" {
  default     = {}
  type        = map(string)
  description = "tags for Nginx vpc"
}
