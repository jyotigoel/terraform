variable "ami" {
  type        = string
  description = "ami id for Nginx instances"
  default     = ""
}
variable "ec2_tags" {
  type        = map(string)
  description = "tags for Nginx manager instance"
  default = {
    Name    = "OS-Manager",
    Owner   = "Jyoti Goel",
    purpose = "os-manager"
  }
}
variable "security_group" {
  type        = string
  description = "public sg for Nginx instances"
  default     = ""
}
variable "subnet" {
  type        = string
  description = "public subnet for Nginx instances"
  default     = ""
}
variable "key_id" {
default = ""
}
