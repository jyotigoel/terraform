variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "os-engine-public-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for Nginx"
  type        = map(string)
}
variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Nginx vpc"
}
