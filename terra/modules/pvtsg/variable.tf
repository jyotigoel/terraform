variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Nginx vpc"
}
variable "sg_pvt" {
  type        = string
  description = "private security group"
  default     = "os-engine-pvt-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "pvt_sg_tags" {
  default     = {}
  description = "Prvate Security group tags for OpenSearch"
  type        = map(string)
}
variable "cidr" {
  default     = {}
}
