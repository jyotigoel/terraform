variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Nginx vpc"
}
variable "vpc_tags" {
  default     = {}
  type        = map(string)
  description = "tags for Nginx vpc"
}
variable "public_subnet_cidr" {
  type        = string
  description = "CIDR block for subnet"
  default     = ""
}
variable "public_subnet_tags" {
  default     = {}
  description = "tags for Nginx subnet"
  type        = map(string)
}
variable "public_subnet_zone" {
  default = {}
}
variable "private_subnet1_cidr" {
  type        = string
  description = "CIDR block for subnet1"
  default     = ""
}
variable "private_subnet1_tags" {
  default     = {}
  description = "tags for Nginx subnet1"
  type        = map(string)
}
variable "private_subnet2_cidr" {
  type        = string
  description = "CIDR block for subnet2"
  default     = ""
}
variable "private_subnet2_tags" {
  default     = {}
  description = "tags for Nginx subnet2"
  type        = map(string)
}
variable "private_subnet1_zone" {
  default = {}
}
variable "private_subnet2_zone" {
  default = {}
}
variable "sg_pvt" {
  type        = string
  description = "private security group"
  default     = "os-engine-pvt-sg"
}
variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "os-engine-public-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "pvt_sg_tags" {
  default     = {}
  description = "Prvate Security group tags for Nginx"
  type        = map(string)
}
variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for Nginx"
  type        = map(string)
}
variable "private_routeTable_tags" {
  description = "tags for Nginx pvt_rtTable vpc"
  default = {
    Name    = "os-private-routeTB",
    Owner   = "Jyoti Goel",
    purpose = "os data node"

  }
}
variable "public_routeTable_tags" {
  description = "tags for Nginx vpc"
  default = {
    Name    = "os-pb-routeTB",
    Owner   = "Jyoti Goel",
    purpose = "nginx-manager"
  }
}
variable "NAT_tags" {
  description = "nat-tags for Nginx vpc"
  default = {
    Name    = "nginx-nat",
    Owner   = "Jyoti Goel",
    purpose = "nginx-nat"
  }
}
variable "igw_tags" {
  description = "tag for internet gateway of os-vpc"
  default = {
    Name  = "nginx-igw"
    Owner = "Jyoti Goel"
  }
}
variable "ami" {
  type        = string
  description = "ami id for Nginx instances"
  default     = ""
}
variable "manager_tags" {
  type        = map(string)
  description = "tags for Nginx data-node1 instance"
  default = {}
}
variable "node1_tags" {
  type        = map(string)
  description = "tags for Nginx data-node1 instance"
  default = {}
}
variable "node2_tags" {
  type        = map(string)
  description = "tags for Nginx data-node1 instance"
  default = {}
}
variable "access_key" {
default = ""
}
variable "key_path" {
default = ""
}
